Basic TDD in Rails : Cleanup

Objective
=========
---------

To delete unnecessary files in our codebase.

Steps
=========
---------

Step 1
-------

Run : 

```sh
$rake spec.
```

Step 2
-------

Open coverage/index.html. We have 100% coverage.

Step 3
-------

As you look at the reports, you can see we don't need some of the files like welcome_helper.rb and article_helper.rb. We can delete them if we don't need them.

You may be thinking, 'Why did we not write any tests for the show and delete articles?'. I wrote only feature specs. The feature specs is mostly used to cover the happy path. We also don't want overlapping tests. We want to test a particular thing only once. We want minimal number of tests that can give us confidence about our code.

Step 4
----------

Delete articles_helper and welcome_helper.rb and the corresponding specs. Delete welcome/index.html.erb_spec.rb

Summary
=========
---------

We cleaned up our codebase by deleting rails generated unnecessary files.