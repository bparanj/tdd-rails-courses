require 'rails_helper'

describe SubscriptionsController, type: :controller do

  
  describe "GET #new" do
    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
    end
    
    it 'should initialize the plan name' do
      get :new, { plan_name: 'gold' }
      
      expect(assigns[:plan_name]).to eq('gold')
    end
    
    it 'should use stripe layout' do      
      get :new, { plan_name: 'gold' }
      
      expect(response).to render_template(layout: 'stripe')
    end
  end

  describe "GET #create" do
    
    it 'should save the subscription details', focus: true do
      Stripe.api_key = 'sk_test_watHqWl2XD88WHqhknk7sqXN'
      Stripe.api_version = "2015-02-18"  
      
      user = User.create(email: 'bugs@disney.com', password: '12345678')
      sign_in(User.last)   
      
      expect(Subscription).to receive(:save_details)
      
      post :create, params: { stripeToken: 1, plan_name: 'gold' }      
    end
  end
  
end
