require 'rails_helper'
require 'spec_helper'

describe ArticlesController, type: :controller do
  
  it "returns http success" do
    get :index
    expect(response).to have_http_status(:success)
  end
  
  it 'should get all the articles' do
    Article.create(title: 'test', description: 'test')
    
    get :index
    
    expect(assigns[:articles].size).to eq(1)
  end

  it 'should not load new article page' do
    article = Article.create(title: 'test', description: 'test')
    
    get :show, { id: article.id }
    
    expect(assigns[:article]).to_not be_nil 
  end

  context 'Admin not logged in' do
    it 'should not create a new article when admin is not logged in' do
      expect do
        post :create, { article: { title: 'test', description: 'test' }}
      end.to_not change(Article, :count)
    end
      
    it 'should not display the edit article page' do
      article = Article.create(title: 'test', description: 'test')
      get :edit, { id: article.id }
      
      expect(response.code).to eq('401')
    end
    
    it 'should return unauthorized status code for update article' do
      article = Article.create(title: 'test', description: 'test')
      put :update, { id: article.id, article: { title: 'updated test' }}
      
      expect(response.code).to eq('401')
    end
    
    it 'should not delete the article' do
      article = Article.create(title: 'test', description: 'test')
      delete :destroy, { id: article.id }
      
      expect(Article.count).to eq(1)  
    end
    
    it 'should not load new article page' do
      get :new
      
      expect(response.code).to eq('401')
    end
        
  end
  
  context 'Admin Logged In' do
    before(:each) do
      http_basic_auth  
    end
  
    it 'should create a new article when admin is logged in' do
      expect do
        post :create, { article: { title: 'test', description: 'test' }}
      end.to change(Article, :count)
    end
    
    it 'should redirect to articles index page after creating the article' do
      post :create, { article: { title: 'test', description: 'test' }}
    
      expect(response).to redirect_to articles_path
    end
      
    it 'should render the new article page if the validation fails' do
      post :create, { article: { title: 'test' }}
    
      expect(response).to render_template(:new)
    end
    
    it 'should render edit page' do
      article = Article.create(title: 'test', description: 'test')
      get :edit, { id: article.id }
      
      expect(response.code).to eq('200')
    end
    
    it 'should update the article' do
      article = Article.create(title: 'test', description: 'test')
      put :update, { id: article.id, article: { title: 'updated test' }}
      
      article = Article.last
      
      expect(article.title).to eq('updated test')
    end
    
    it 'should delete the article' do
      article = Article.create(title: 'test', description: 'test')
      delete :destroy, { id: article.id }
      
      expect(Article.count).to eq(0)  
    end
    
    it 'should load new article page' do
      get :new
      
      expect(response.code).to eq('200')
    end
    
    
  end
  
  
end
