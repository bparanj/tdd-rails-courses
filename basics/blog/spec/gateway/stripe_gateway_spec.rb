require 'rails_helper'
require 'spec_helper'

describe StripeGateway::Subscription do
  before do
    Stripe.api_key = 'sk_test_watHqWl2XD88WHqhknk7sqXN'
    Stripe.api_version = "2015-02-18"
  end
  
  it 'should create a customer' do
    VCR.use_cassette "create customer" do
      token = Stripe::Token.create(
        card: {
          number: "4242424242424242",
          exp_month: 3,
          exp_year: 2016,
          cvc: "314"
        }
      )
    
      customer = StripeGateway::Subscription.create_customer(token.id, 'description')
    
      expect(customer.id.size).to be > 2 
    end
  end
  
  it 'should subscribe a customer to a given plan' do
    VCR.use_cassette "create subscription" do
      stripe_plan_id = 'test'
      token = Stripe::Token.create(
        card: {
          number: "4242424242424242",
          exp_month: 3,
          exp_year: 2016,
          cvc: "314"
        }
      )
      customer = StripeGateway::Subscription.create_customer(token.id, 'description')
      stripe_customer_id = customer.id
    
      subscription = StripeGateway::Subscription.create(stripe_customer_id, stripe_plan_id)
    
      expect(subscription.id.size).to be > 2
    end
  end
end