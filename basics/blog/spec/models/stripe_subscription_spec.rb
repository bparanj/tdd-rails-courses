require 'rails_helper'

describe StripeSubscription, type: :model do
  it 'should save subscription details' do
    stripe_customer = 'sk_007'
    plan_name = 'diamond'
    user = User.new
    
    result = StripeSubscription.save(stripe_customer, user, plan_name) 
    
    expect(result).to be true
  end

end