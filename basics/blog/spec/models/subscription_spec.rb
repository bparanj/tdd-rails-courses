require 'rails_helper'

describe Subscription do
  it 'belongs to user' do
    subscription = Subscription.new
    
    expect(subscription.respond_to?(:user)).to be true
  end
  
  it 'should save the stripe subscription details' do
    h = JSON.parse(File.read("spec/fixtures/customer.json"))
    stripe_customer = Stripe::Customer.construct_from(h)    
    user = User.create(email: 'bugs@rubyplus.com', password: '12345678', password_confirmation: '12345678')
    plan_name = 'gold'
    
    expect do
      Subscription.save_details(stripe_customer, user, plan_name)
    end.to change(Subscription, :count).by (1)
    
  end
  
end
