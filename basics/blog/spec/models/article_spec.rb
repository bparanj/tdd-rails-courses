require 'rails_helper'

describe Article, type: :model do
  it 'is valid if title and description fields have value' do
    expect do
      article = Article.new(title: 'test', description: 'test')
      article.save
    end.to change{Article.count}.by(1)
  end
  
  it 'is not valid if it has no title' do
    article = Article.new(description: 'test')
    article.save
    
    expect(article.errors[:title]).to eq(["can't be blank"])
  end

  it 'is not valid if it has no description' do
    article = Article.new(title: 'test')
    article.save
    
    expect(article.errors[:description]).to eq(["can't be blank"])
  end
  
end
