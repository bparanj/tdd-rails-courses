require 'rails_helper'
require 'spec_helper'

feature "Subscribe", :type => :feature do
  before do
    Stripe.api_key = 'sk_test_watHqWl2XD88WHqhknk7sqXN'
    Stripe.api_version = "2015-02-18"
  end
  
  scenario "to gold plan" do
    visit root_path

    sign_up('bugs@rubyplus.com', '12345678')
    
    click_link 'Gold'
    fill_in "Card Number", with: '4242424242424242'
    page.select '10', from: "card_month"
    page.select '2029', from: 'card_year'
    click_button 'Subscribe Me'
    sleep 4
    expect(page).to have_text("You are now on gold plan.")    
  end

  scenario "to a plan saves the subscription details", js: true, focus: true do
    expect do
      visit root_path

      sign_up('bugs2@rubyplus.com', '12345678')
    
      click_link 'Gold'
      fill_in "Card Number", with: '4242424242424242'
      page.select '10', from: "card_month"
      page.select '2029', from: 'card_year'
      click_button 'Subscribe Me'
      sleep 4
    end.to change(Subscription, :count).by(1)
  end
  
  
end