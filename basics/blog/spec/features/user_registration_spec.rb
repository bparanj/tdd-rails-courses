require 'rails_helper'
require 'spec_helper'

feature "User Registration", :type => :feature do
  scenario "success" do
    visit root_path

    click_link 'Register'
    fill_in 'Email', with: 'bugs@rubyplus.com'
    fill_in 'Password', with: '12345678'
    
    click_button 'Sign up'

    expect(page).to have_text("You have signed up successfully.")
  end
  
end