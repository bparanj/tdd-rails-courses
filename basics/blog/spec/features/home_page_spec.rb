require 'rails_helper'
require 'spec_helper'

feature "Home Page", :type => :feature do
  scenario "must load" do
    visit root_path

    expect(page).to have_text("My Blog")
  end
  
  scenario 'has link to my blog' do
    visit root_path
    
    click_link 'My Blog'
    
    expect(page).to have_text('Listing Articles')  
  end
end