module ControllerHelpers
  def http_basic_auth
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials('user','secret')
  end  
end