module Features
  module SessionHelpers
    def sign_up(email, password)
      click_link 'Register'
      fill_in 'Email', with: email
      fill_in 'Password', with: password
      click_button 'Sign up'
    end
  end
end


