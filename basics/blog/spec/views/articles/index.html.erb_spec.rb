require 'rails_helper'

describe "articles/index.html.erb", type: :view do
  it 'displays all the articles' do
    assign(:articles, [
      Article.create!(title: 'test', description: 'description'),
      Article.create!(title: 'test2', description: 'description2')
    ])
    
    render
    
    expect(rendered).to match /test/
    expect(rendered).to match /test2/
  end
end
