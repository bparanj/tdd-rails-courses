class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string   :plan_name
      t.string   :stripe_customer_token
      t.string   :last4digits, limit: 4
      t.integer  :expiration_month, limit: 4
      t.integer  :expiration_year,  limit: 4

      t.integer  :user_id
      
      t.timestamps null: false
    end
  end
end
