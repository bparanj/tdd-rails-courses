module StripeGateway

  class Subscription
    
    def self.create(email, stripe_token, stripe_plan_id)
      Stripe::Customer.create(description: email, card: stripe_token, plan: stripe_plan_id)      
    end
    
  end
  
end