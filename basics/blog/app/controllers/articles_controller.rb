class ArticlesController < ApplicationController
  http_basic_authenticate_with name: 'user', password: 'secret', only: [:create, :edit, :update, :destroy, :new]
    
  def index
    @articles = Article.all
  end
  
  def new
    @article = Article.new
  end
  
  def create
    allowed_params = params.require(:article).permit(:title, :description)
    @article = Article.new(allowed_params)
    
    if @article.save
      redirect_to articles_path
    else
      render :new
    end
  end
  
  def edit
    @article = Article.find(params[:id])
  end
  
  def update
    allowed_params = params.require(:article).permit(:title, :description)
    article = Article.find(params[:id])
    article.update_attributes(allowed_params)
    
    redirect_to articles_path
  end
  
  def show
    @article = Article.find(params[:id])
  end
  
  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    
    redirect_to articles_path
  end
end
