class SubscriptionsController < ApplicationController
  layout 'stripe'  
  
  def new
    @plan_name = params[:plan_name]      
  end

  def create    
    stripe_customer = StripeGateway::Subscription.create(current_user.email, params[:stripeToken], params[:plan_name])
    Subscription.save_details(stripe_customer, current_user, params[:plan_name])
  end
end
