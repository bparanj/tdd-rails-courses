class Subscription < ActiveRecord::Base
  belongs_to :user  
  
  def self.save_details(stripe_customer, user, plan_name)
    mapper = StripeCustomerMapper.new(stripe_customer)
    user.create_subscription(stripe_customer_token: mapper.customer_token,
                             plan_name:             plan_name,
                             last4digits:      mapper.credit_card_last4digits,
                             expiration_month: mapper.credit_card_expiration_month,
                             expiration_year:  mapper.credit_card_expiration_year)
  end
end
