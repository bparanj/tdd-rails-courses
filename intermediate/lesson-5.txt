Intermediate TDD in Rails : Install and Configure Devise

Objective
=========
---------

Install and configure devise.

Requirement
=========
---------

When a user registers and logs in to our site, we will present the different subscription plans they can choose from. This lesson will focus on devise installation and configuration. We will write tests and drive the implementation in the next lesson.

Steps
=========
---------

Step 1
-------

Add

```ruby
gem 'devise' 
```

to Gemfile. Run bundle.

Step 2
-------

Run:

```ruby
$rails g devise:install
```

Step 3
-------

Add: 

```ruby
config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }
```

to config/environments/development.rb

Step 4
-------

Add : 

```ruby
  <p class="notice"><%= notice %></p>
  <p class="alert"><%= alert %></p>
```

to app/views/layouts/application.html.erb file. The layout file now looks like this:

```ruby
<!DOCTYPE html>
<html>
<head>
  <title>Blog</title>
  <%= stylesheet_link_tag    'application', media: 'all', 'data-turbolinks-track' => true %>
  <%= javascript_include_tag 'application', 'data-turbolinks-track' => true %>
  <%= csrf_meta_tags %>
</head>
<body>

   <p class="notice"><%= notice %></p>
   <p class="alert"><%= alert %></p>

<%= yield %>

</body>
</html>
```

Step 5
-------

Copy devise views for customization.

```ruby
$rails g devise:views
```

Step 6
-------

Create the user model.

```ruby
$rails g devise user
```

Step 7
-------

Create the table.

```ruby
$rake db:migrate
```

Summary
=========
-----------

In this lesson we installed and configured devise to manage user accounts.
